package com.qaagility.controller;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AboutTest {

    @Test
    public void testDesc() {
        String message = "This application was copied from somewhere, sorry but I cannot give the details and the proper copyright notice. Please don't tell the police!";
        assertEquals("Description", message, new About().desc());
    }

}
