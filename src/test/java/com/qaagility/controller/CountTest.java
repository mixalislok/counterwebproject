package com.qaagility.controller;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CountTest {

    @Test
    public void testDivide() {
        assertEquals("divide zero", Integer.MAX_VALUE, new Count().numberss(4, 0));
    }

    @Test
    public void testDivide_zero() {
        assertEquals("divide zero", 3, new Count().numberss(6, 2));
    }
}
